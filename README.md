# BBS

#### 介绍
      一个基于 ThinkPHP5.1 + Vue 的轻社区论坛系统的实例的开发过程。系统采用运用前后端分离技术，后端采用ThinkPHP5开发框架提供后端API接口，前端利用Vue.js、Vuex、Vue Router和Axios等技术实现，以VScode作为开发平台，数据库采用的是Mysql5.7。
       系统开发中所使用的关键技术，包括前端所用到的Vue.js、Vuex、Vue Router、Axios、Bootstrap、Font Awesome等技术，后端所用到的Apache、MySQL、Redis、ThinkPHP、面对对象建模技术等。


